package xyz.adriano.soapemon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.adriano.soapemon.model.RequestDetail;

public interface RequestRepository  extends JpaRepository<RequestDetail, Long>  {  
    
}
