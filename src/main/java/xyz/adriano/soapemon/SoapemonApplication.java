package xyz.adriano.soapemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoapemonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoapemonApplication.class, args);
	}

}
