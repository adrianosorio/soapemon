package xyz.adriano.soapemon.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import xyz.adriano.soapemon.model.Pokemon;
import xyz.adriano.soapemon.model.RequestDetail;
import xyz.adriano.soapemon.repository.RequestRepository;
import xyz.adriano.soapemon.rest.RestClient;

@Component
public class PokemonService {
    
    private RestClient restClient;
    private RequestRepository requestRepository;

    @Autowired
    public PokemonService(RestClient restClient, RequestRepository requestRepository){
        this.restClient = restClient;
        this.requestRepository = requestRepository;
    } 
    
    private Pokemon pokemon;

    public Pokemon processRequest(String ipAddress, String pokemonName, String method){
        RequestDetail detail = new RequestDetail();
        detail.setIpOrigin(ipAddress);
        detail.setRequestDate(new Date());
        detail.setMethod(method);

        pokemon = restClient.getPokemon(pokemonName);
        requestRepository.save(detail);
        
        return pokemon;
    }

}
