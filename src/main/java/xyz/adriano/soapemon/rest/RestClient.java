package xyz.adriano.soapemon.rest;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import xyz.adriano.soapemon.model.Pokemon;

@Component
public class RestClient {
    
    final String POKEAPI_URI = "https://pokeapi.co/api/v2/pokemon/";

    public Pokemon getPokemon(String name)
    {
        
        RestTemplate restTemplate = new RestTemplate();

        Pokemon pokemon = restTemplate.getForObject(POKEAPI_URI+name, Pokemon.class);
        return pokemon;
    }

}
