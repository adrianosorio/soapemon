package xyz.adriano.soapemon.ws;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import xyz.adriano.soapemon.model.GetAbilitiesReq;
import xyz.adriano.soapemon.model.GetAbilitiesRsp;
import xyz.adriano.soapemon.model.GetExpirenceReq;
import xyz.adriano.soapemon.model.GetExpirenceRsp;
import xyz.adriano.soapemon.model.GetHeldItemsReq;
import xyz.adriano.soapemon.model.GetHeldItemsRsp;
import xyz.adriano.soapemon.model.GetIdReq;
import xyz.adriano.soapemon.model.GetIdRsp;
import xyz.adriano.soapemon.model.GetLocationReq;
import xyz.adriano.soapemon.model.GetLocationRsp;
import xyz.adriano.soapemon.model.GetNameReq;
import xyz.adriano.soapemon.model.GetNameRsp;
import xyz.adriano.soapemon.model.Pokemon;
import xyz.adriano.soapemon.service.PokemonService;

@Endpoint
public class PokemonEndpoint {
    private static final String NAMESPACE_URI = "http://adriano.xyz/soapemon";

	private Pokemon pokemon;
	private PokemonService pokemonService;
    private HttpServletRequest httpRequest;

	@Autowired
	public PokemonEndpoint(PokemonService pokemonService, HttpServletRequest httpRequest) {
		this.pokemonService = pokemonService;
		this.httpRequest = httpRequest;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getIdReq")
	@ResponsePayload
	public GetIdRsp getId(@RequestPayload GetIdReq request) {
		GetIdRsp response = new GetIdRsp();
		pokemon = pokemonService.processRequest(httpRequest.getRemoteAddr(), request.getName(), "getIdReq");
		response.setId(pokemon.getId());
		return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getNameReq")
	@ResponsePayload
	public GetNameRsp getName(@RequestPayload GetNameReq request) {
		GetNameRsp response = new GetNameRsp();
		pokemon = pokemonService.processRequest(httpRequest.getRemoteAddr(), request.getName(), "getNameReq");
		response.setName(pokemon.getName());
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getExpirenceReq")
	@ResponsePayload
	public GetExpirenceRsp getExpirence(@RequestPayload GetExpirenceReq request) {
		GetExpirenceRsp response = new GetExpirenceRsp();
		pokemon = pokemonService.processRequest(httpRequest.getRemoteAddr(), request.getName(), "getExpirenceReq");
		response.setExpirence(pokemon.getBaseExperience());
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getLocationReq")
	@ResponsePayload
	public GetLocationRsp getLocation(@RequestPayload GetLocationReq request) {
		GetLocationRsp response = new GetLocationRsp();
		pokemon = pokemonService.processRequest(httpRequest.getRemoteAddr(), request.getName(), "getLocationReq");
		response.setLocation(pokemon.getLocationAreaEncounters());
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getHeldItemsReq")
	@ResponsePayload
	public GetHeldItemsRsp getHeldItems(@RequestPayload GetHeldItemsReq request) {
		GetHeldItemsRsp response = new GetHeldItemsRsp();
		pokemon = pokemonService.processRequest(httpRequest.getRemoteAddr(), request.getName(), "getHeldItemsReq");
		response.getHeldItems().addAll(pokemon.getHeldItems());
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAbilitiesReq")
	@ResponsePayload
	public GetAbilitiesRsp getAbilities(@RequestPayload GetAbilitiesReq request) {
		GetAbilitiesRsp response = new GetAbilitiesRsp();
		pokemon = pokemonService.processRequest(httpRequest.getRemoteAddr(), request.getName(), "getAbilitiesReq");
		response.getAbilities().addAll(pokemon.getAbilities());
		return response;
	}

}
